import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { initFireStore } from './fireabase/config';
import './index.css'


initFireStore()

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)


