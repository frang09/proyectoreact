import { useState } from "react"
import { useCartContext } from "../../Context/CartContext"
import ItemCount from "../ItemCount/ItemCount"
import './ItemDetail.css'


const ItemDetail = ({ product }) => {

    const [cart, setCart] = useState(false)

    const { addCart, cancelOrder } = useCartContext()


    
    const onAdd = (cant)=>{
      addCart( { ...product, cantidad: cant } )
      cancelOrder()
    }  

    const handleCart = () => {
        setCart(!cart)
    }

    
    return (    
      <div className="art">
            <div className="side-a">
              <div className='art-titulo'>
              {product.cat} - {product.name}
              </div>
                <img src={product.img} alt='Image' className="art-img"/>
              </div>
              
                <div className='side-b ms-4'>
                  <div className="name">
                    <h2>{product.name}</h2>
                  </div>
                  <div>
                    <p>cod: {product.id}</p>
                  </div>
                  <div className="stock-art">
                    <p>stock: {product.stock}</p>
                  </div>

                  <div className='art-precio mt-2 fw-bolder fs-2 text-warning'>
                  ${`${product.price}`}
                  </div>

                  <div className="row ">
                      <div className="col-6 offset-4">
                        <div className="quantity">
                          <ItemCount initial={1} stock={product.stock} onAdd={onAdd} />                     
                        </div>
                      </div>
                    </div>

                </div>
                
      </div>
          
  
  )
}

export default ItemDetail