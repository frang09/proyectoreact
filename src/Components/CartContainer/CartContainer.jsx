import { useCartContext } from "../../Context/CartContext"
import CartList from "../CartList/CartList"
import EmptyCart from "../EmptyCart/EmptyCart"
import OrderGenerator from "../OrderGenerator/OrderGenerator"
import OrderSuccess from "../OrderSuccess/OrderSuccess"

    const CartContainer = () => {

    const {orderState, totalQuant} = useCartContext()

    console.log(orderState)

    return (
            <>

            { 

            orderState === 3? 
            <OrderSuccess/> : totalQuant() > 0? 
            orderState === 1 ?  <CartList/> :<OrderGenerator/>
            :
            <EmptyCart/>
            }

            </>
    )
}

export default CartContainer