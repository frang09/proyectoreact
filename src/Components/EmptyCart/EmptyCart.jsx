import React from 'react'
import { Link, NavLink } from 'react-router-dom';


const EmptyCart = () => {
  return (

        <div className='text-center mt-5'>
            <h2>El carrito está vacio :/</h2>
            <h3>Te invitamos a revisar nuestro catálogo</h3>  
            <Link to= '/'>
                <button className='btn btn-outline-success'>Ir al Catalogo</button>
            </Link>
        </div>       
    )
}

export default EmptyCart