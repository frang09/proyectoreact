import React from 'react'
import { useState } from "react"


const ItemCount = ({initial, stock, onAdd}) => {
  const [ count, setCount] = useState(initial)

  const handleAdd = () =>{
      if(count < stock){
          setCount(count + 1)
      }
  }
  const handleSubstract = () =>{
      if(count > initial){
          setCount(count - 1)
      }
  }
  const handleOnAdd = ()=>{
    onAdd(count)        
}

return (
  <div>
        <div className=" ms-1 row">
          <div className="col-3">
          <button className="btn btn-outline-dark w-100" onClick={handleAdd}> + </button>

          </div>
          <div className="col-2 mt-1 fw-bolder">
          <center>
          <label>{count}</label>
          </center> 
          </div>
          <div className="col-3">
          <button className="btn btn-outline-dark w-100" onClick={handleSubstract }> - </button>
          </div>
         </div>
          <div className="row">
          <div className="col">
          </div>


          <button className='btn btn-success me-5 mt-2 w-75' onClick={handleOnAdd}>AGREGAR AL CARRO</button>

      </div>


  </div>
)
}

export default ItemCount