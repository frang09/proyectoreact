import React from 'react'
import { useCartContext } from "../../Context/CartContext"
import './CartList.css'


const CartList = () => {

    const {cartList, totalPrice,  removeProduct, emptyCart, generateOrder, cancelOrder} = useCartContext() 

    
  return (
<div className="container mt-5">

<div className="row">
        <div className="col-3 text-center"> Imagen</div>
        <div className="col-3 text-center"> Nombre </div>
        <div className="col-1 text-center"> Cantidad </div>
        <div className="col-2 text-center"> Precio</div>
        <div className="col-1 text-center"> Subtotal</div>
    </div>
    <div className='mt-3 listContainer'>
{ cartList.map(product => (
    <div >

    <div className="row btn-outline-secondary listContainerItem " key={product.id} >
        <div className="col-3 text-center pt-1"> <img src={product.img} style={{width: 75}} /></div>
        <div className="col-3 text-center listContainerItemGray pt-4"> {product.name} </div>
        <div className="col-1 text-center pt-4"> {product.cantidad} </div>
        <div className="col-2 text-center listContainerItemGray pt-4"> {product.price} </div>
        <div className="col-1 text-center pt-4"> {product.cantidad*product.price}</div>
        <div className='col-1 '>
        <button className="btn btn-danger mt-3 ms-3" onClick={()=> removeProduct(product.id)} style={{width: 50, height: 50}}>X</button>
        </div>
    </div>
        
    </div>
      
))}
    </div>
<div className="row">
    <button className="col-2 offset-1 btn btn-outline-danger" onClick={emptyCart}>Vaciar Carrito</button>
    <p className="col-2 offset-5 align-self-end fw-bolder fs-5">{totalPrice() !== 0 && `TOTAL: $${totalPrice()}`}</p>
    <button className="col-3 offset-4 btn btn-outline-success mt-5" onClick={generateOrder} >GENERAR ORDEN DE COMPRA</button>
</div>
</div>
)
}

export default CartList