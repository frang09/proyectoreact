import React from 'react'
import { useCartContext } from "../../Context/CartContext"
import { useState } from "react"
import { addDoc, collection, doc, getFirestore, updateDoc } from "firebase/firestore"
import FormError from '../FormError/FormError'



const OrderGenerator = () => {

    const {cartList, emptyCart, totalPrice, completedOrder,setlastOrderID} = useCartContext() 

    const [error, setError] = useState(false)
    const [formData, setFormData] = useState( {
        name: '',
        phone: '',
        adress1: '',
        adress2: '',
        email:'',
        repeatEmail: ''
    } )

    const generateOrder = (evt) => {
        evt.preventDefault()

        if (
            formData.adress2.length > 0 &&
            formData.adress1.length > 0 &&
            formData.phone.length>0 &&
            formData.repeatEmail.length > 0 &&
            formData.email.length > 0 &&
            formData.adress1.length > 0 &&
            formData.email === formData.repeatEmail 
            
            ) {
            const order = {}
            order.buyer = formData
            order.products = cartList.map( ({id, name, price}) => ({id, name, price}))
            order.total = totalPrice()
    
    
            const db = getFirestore()
            const ordersCollection = collection(db, 'orders')
            addDoc(ordersCollection, order)
            .then(resp => setlastOrderID(resp.id))
            .catch(err => console.log(err))
            .finally(() => {
                completedOrder()
                emptyCart()
                setFormData({
                    name: '',
                    phone: '',
                    adress1: '',
                    adress2: '',
                    email:'',
                    repeatEmail: ''
                })
                
                setError(false)
            })
        }else{
            setError(true)
        }


  
    }

    const handleOnChange = (evt) => {
        setFormData({
            ...formData,
            [evt.target.name]: evt.target.value
        })
    }

  return (
            <div>
                <h2 className="text-center">CREACION DE ORDEN</h2>
                <form onSubmit={generateOrder} className='container-fluid text-center'>
                    <input className="m-3" 
                        type="text" 
                        name="name"          
                        placeholder = "Ingrese el nombre"   
                        onChange={handleOnChange} 
                        value={formData.name}

                    />
                        <input className="m-3" 
                        type="email" 
                        name="email"         
                        placeholder = "Ingrese el email"    
                        onChange={handleOnChange} 
                        value={formData.email}
                        /><br/>
                    <input className="m-3" 
                        type="tel" 
                        name="phone"         
                        placeholder = "Ingrese el teléfono" 
                        onChange={handleOnChange} 
                        value={formData.phone}

                        

                    />
                    
                    <input className="m-3" 
                        type="email" 
                        name="repeatEmail"  
                        placeholder = "Repetir el email"    
                        onChange={handleOnChange} 
                        value={formData.repeatEmail}

                    /><br />
                    <input className="m-3" 
                        type="text" 
                        name="adress1"         
                        placeholder = "Ingrese direccion (calle)"    
                        onChange={handleOnChange} 
                        value={formData.adress1}

                    />
                    <input className="m-3" 
                        type="tel" 
                        name="adress2"         
                        placeholder = "Ingrese direccion (numero)"    
                        onChange={handleOnChange} 
                        value={formData.adress2}

                    /><br />
                   
                    <button className="btn btn-outline-success" type="submit" >Generar la orden</button>
                   

                   {
                      error && <FormError/>
                    
                   }
            </form>

            </div>


    )
}

export default OrderGenerator