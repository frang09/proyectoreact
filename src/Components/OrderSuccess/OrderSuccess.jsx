import React from 'react'
import { useCartContext } from "../../Context/CartContext"


const OrderSuccess = () => {

  const {cancelOrder, lastOrderID} = useCartContext() 
    return (

    <div className='text-center mt-5'>
      <h2>Felicitaciones!</h2>
      <p>Su orden de compra fué generada correctamente</p>
      <p>ID: {lastOrderID}</p>
      <button className='btn btn-outline-success' onClick={cancelOrder}>Ver el carrito</button>
    </div>
  )
}

export default OrderSuccess