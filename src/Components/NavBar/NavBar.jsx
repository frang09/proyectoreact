import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { CartWidget } from '../CartWidget/CartWidget';
import { useCartContext } from "../../Context/CartContext"


import { Link, NavLink } from 'react-router-dom';



function NavBar() {

  const {totalQuant} = useCartContext()

  
  return (
    <Navbar  expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Collapse className=''>
          <NavLink to="/categoria/casuales" className={({ isActive })=> isActive  ? 'btn btn-light m-1':'btn btn-outline-light m-1' } >Casuales</NavLink>
          <NavLink to="/categoria/deportivas" className={({ isActive })=> isActive  ? 'btn btn-light m-1':'btn btn-outline-light m-1' } >Deportivas</NavLink>
          <NavLink to="/categoria/importadas" className={({ isActive })=> isActive  ? 'btn btn-light m-1':'btn btn-outline-light m-1' } >Importadas</NavLink>
        </Navbar.Collapse>

        <Navbar.Collapse className='position-absolute top-50 start-50 translate-middle'>
        <NavLink to='/' style={{textDecoration: 'none'}} >
          <Navbar.Brand > 
            <img
                alt=""
                src="https://img.icons8.com/sf-regular-filled/38/trainers.png"
                width="30"
                height="30"
                className="d-inline-block align-top"/> 
              TeShoes
          </Navbar.Brand>
        </NavLink>
        </Navbar.Collapse>


        <Link to= '/cart' style={{textDecoration: 'none'}}>
          <CartWidget />
        </Link>
        <Nav>
          <h2 className='text-warning fs-5 mt-2 ms-2'>  
          {totalQuant() !== 0 && totalQuant()}
          </h2>
        </Nav>
      </Container>
    </Navbar>
  );
}

export default NavBar;