import React from 'react'

const Error = () => {
  return (
    <div className='text-center mt-5 text-danger'>
        <h2>ERROR 404</h2>
        <h3>La ruta a la que desea acceder no existe</h3>
    </div>
  )
}

export default Error