import React from 'react'

const FormError = () => {
  return (
    <div className='alert alert-danger w-50 mt-5 start-50 translate-middle'>
        Por favor, verifique el formulario antes de intentar nuevamente
    </div>
  )
}

export default FormError