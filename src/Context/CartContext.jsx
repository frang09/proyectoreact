import { createContext, useState, useContext } from "react";

const CartContext = createContext([])

export const useCartContext = () =>  useContext(CartContext)

export const CartContextProvider = ({children})=>{
    const [ cartList, setCartList ] = useState([])
    const [orderState, setOrder ] = useState(1)
    const [lastOrderID, setlastOrderID ] = useState("")

    const generateOrder = () => {
        setOrder(2);
    }
    const cancelOrder= () => {
        setOrder(1);
    }
    const completedOrder = () => {
        setOrder(3);
    }
    const addCart = (newProduct) =>{
        const idxProd = cartList.findIndex(product => product.id === newProduct.id)
        cancelOrder();
        if(idxProd!==-1){
            cartList[idxProd].cantidad += newProduct.cantidad
            setCartList([...cartList])
            return 
        }

        setCartList( [
            ...cartList,
            newProduct
        ] )

    }


    const totalPrice = () => cartList.reduce( (count, producto) => count += (producto.cantidad*producto.price), 0)
    
    const totalQuant = () => cartList.reduce( (count, producto) => count += producto.cantidad, 0)

    const removeProduct = (id) => setCartList(cartList.filter(prod => prod.id !== id))
    

    const emptyCart = () => setCartList( [] )

    return (
        <CartContext.Provider value={{
            cartList,
            addCart,
            totalPrice,
            totalQuant,
            removeProduct,
            emptyCart,
            generateOrder,
            cancelOrder,
            orderState,
            completedOrder,
            lastOrderID,
            setlastOrderID
        
        }}>
            {children}
        </CartContext.Provider>
    )
}