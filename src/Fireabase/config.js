import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCMN9DYmFvBG5bnfOCZccVxS5Y7u45vL70",
  authDomain: "cursoreact-bcba0.firebaseapp.com",
  projectId: "cursoreact-bcba0",
  storageBucket: "cursoreact-bcba0.appspot.com",
  messagingSenderId: "954061302201",
  appId: "1:954061302201:web:3e28ddb18bd49ae66f6e68"
};

const app = initializeApp(firebaseConfig);

export const initFireStore = () => {
    return app
}